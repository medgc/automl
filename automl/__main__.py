import os
import getopt
import sys
import logging
import pandas as pd
from pycaret.classification import *
from lib import STOPWORDS
from lib import tokenizador
from lib import vectorize



DATA_DIR = os.path.join("..", "data")
TRAIN_DATA_FILE = os.path.join(DATA_DIR + '/balanced-37_5K.csv.gz')
VALIDATION_DATA_FILE = os.path.join(DATA_DIR + '/validacion.csv.gz')
TEST_SIZE = 0.3           # tamaño (%) del conjunto de test (test_size) vs entrenamiento (1 - test_size)
MIN_DF= 50 # cantidad minima de docs que tienen que tener a un token para conservarlo.
MAX_DF= 0.8 # cantidad maxima de docs que tienen que tener a un token para conservarlo.
MIN_NGRAMS=1 # n-grams mínimo (1 -> unigramas)
MAX_NGRAMS=2 # n-grams máximo (2 -> bigramas)


#
# Carga de dataframes para train/test y validación
#
def load_datasets():
    df_train = pd.read_csv(TRAIN_DATA_FILE, compression='infer')
    logging.info('Dataset de entrenamiento cargado %s, Categorias: %s.' % (df_train.shape, df_train['section'].unique()))

    df_holdout = pd.read_csv(VALIDATION_DATA_FILE, compression='infer')
    logging.info('Dataset de entrenamiento cargado %s, Categorias: %s.' % (df_holdout.shape, df_holdout['section'].unique()))

    return (df_train, df_holdout)



def pycaret_train(df):
    py_caret = setup(data=df[['vectors', 'section']], target='section', session_id=611, silent=True)
    compare_models(turbo=True, exclude=['qda', 'lda'])
    model = create_model('svm')
    tuned = tune_model(model)
    plot_model(tuned, plot='confusion_matrix')
    evaluate_model(tuned)
    predict_model(tuned)
    final = finalize_model(tuned)
    print(final)
    save_model(final, 'Final SVM')


def show_help(progname):
    help = """
    """
    print(help)

def main(argv):
    progname = os.path.basename(__file__)
    try:
        opts, args = getopt.getopt(argv, 'h', ['help', 'pycaret-train'])
    except getopt.GetoptError:
        show_help(progname)
        sys.exit(1)

    for opt, arg in opts:
        if opt == '-h':
            show_help(progname)
            sys.exit()
        elif opt in ('--pycaret'):
            pycaret_train(df_train, df_holdout)
            df_train, df_holdout = load_datasets()
            df_train['vectors'] = vectorize.tfidf(df_train['article'], tokenizador.tokenizador(), STOPWORDS, MIN_DF, MAX_DF, MIN_NGRAMS, MAX_NGRAMS)
            df_holdout['vectors'] = vectorize.tfidf(df_holdout['article'], tokenizador.tokenizador(), STOPWORDS, MIN_DF, MAX_DF, MIN_NGRAMS, MAX_NGRAMS)
            #test_pycaret(df_train)


if __name__ == '__main__':
    main(sys.argv[1:])


