from typing import Pattern, Optional, List, Tuple, Callable
import re


def tokenizador(token_regex: Optional[Pattern] = None) -> Callable[[str], List[str]]:
    """
    :param token_regex: Una expresion regular que define que es un token
    :return: Una funcion que recibe un texto y retorna el texto tokenizado.
    """
    return lambda doc: tokenizador.token_pattern.findall(doc)

tokenizador.token_pattern = re.compile(r'[a-zA-ZâáàãõáêéíóôõúüÁÉÍÓÚñÑçÇ][0-9a-zA-ZâáàãõáêéíóôõúüÁÉÍÓÚñÑçÇ]+')