import nltk
from nltk.corpus import stopwords

nltk.download('stopwords')

def leer_stopwords(files):
    words = []

    # leemos las palabras de todos los archivos de entrada
    for file in files:
        with open(file, "rt") as swf:
            words.extend([stopword for stopword in [stopword.strip().lower() for stopword in swf] if len(stopword) > 0])

    # agregamos los stopwords nltk
    words.extend(stopwords.words('spanish'))

    # eliminamos duplicados
    return list(set(words))

