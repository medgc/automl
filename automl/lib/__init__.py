from . import stopwords
from . import tokenizador
from . import vectorize
import os


STOPWORDS = stopwords.leer_stopwords(
    [os.path.join(os.path.dirname(__file__), file) for file in ['stopwords_es.txt', 'stopwords_es_sin_acentos.txt']]
)