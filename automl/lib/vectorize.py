from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
import logging


def tfidf(articles, tokenizer, stopwords, min_df, max_df, min_ngrams, max_ngrams, encoding='latin-1'):
    logging.info('Creando representación TF-IDF de los artículos.')
    tfidf_vect = TfidfVectorizer(
        sublinear_tf=True,  # usamos frecuencia logarítimica
        min_df=min_df,
        max_df=max_df,
        norm='l2',
        encoding=encoding,
        ngram_range=(min_ngrams, max_ngrams),
        lowercase=True,
        stop_words=stopwords,
        tokenizer=tokenizer
    )
    return tfidf_vect.fit_transform(articles)

def countv(articles, tokenizer, stopwords, min_df, max_df, min_ngrams, max_ngrams, encoding='latin-1'):
    logging.info('Creando representación Count-Vectorizer de los artículos.')
    count_vect = CountVectorizer(
        min_df=min_df,
        max_df=max_df,
        encoding=encoding,
        ngram_range=(min_ngrams, max_ngrams),
        stop_words=stopwords,
        lowercase=True,
        decode_error='ignore',
        tokenizer = tokenizer,
    )
    return count_vect.fit_transform(articles)


