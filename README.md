# README #

El presente proyecto se provee como Trabajo Final para la materia Fundamentos del Aprendizaje Automatizado. 

En el directorio *data/* pueden encontrarse el archivo *balanced-37_5K.csv.gz* de entrenamiento con 37.500 artículos 
periodísticos, y el archivo *validacion.csv.gz* con 311 para la validación. Ambos archivos se utilizan para entrenar los
modelos y luego validar los resultados a fin de realizar comparaciones. 

## Requerimientos ##
Los scripts provistos utilizan Docker para generar un ambiente homogéneo y obtener así una ejecución controlada. **Es 
requisito indispensable contar con una instalación de Docker en funcionamiento para su correcta ejecución**.


## Estructura del proyecto ###
La estructura básica del proyecto es la siguiente:
* **bin**: contiene los scripts de ejecución de entrenamiento y prueba de modelos, además de nuestro crawler.
* **data**: contiene los sets de datos del proyecto que se usaran para entrenar y validar los modelos.
* **lib**: librerías y módulos comunes reutilizables.

## Set up del proyecto ##
* Clonar el proyecto (`git clone https://bitbucket.org/medgc/webmining.git`)
* Correr los scripts que figuran en la próxima sección.

## Scripts ###
Los scripts aquí documentados pueden encontrarse en el directorio **bin/** de nuestro proyecto.






## Estructura del código fuente ###
